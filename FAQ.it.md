# Domande frequenti

## Previsioni meteo

### Da dove arrivano le previsioni?

Le previsioni vengono generate da [OpenWeather][openweather-website] e vengono
lette tramite le API messe a disposizione dal servizio:

- [Previsioni meteo][openweather-one-call-api]
- [Previsioni inquinamento aria][openweather-air-pollution-api]
- [Dati geografici][openweather-geocoding-api]

### Quali dati vengono mostrati nel messaggio Telegram?

![Messaggio Telegram][images-message]

Supponendo che il messaggio sia inviato alle 7 del mattino,
le informazioni mostrate sono le seguenti:

- La somma delle precipitazioni previste nella giornata (dalle 7 alle 23).
  - Nel caso in cui la somma sia zero, si mostra l'etichetta "assenti".
  - Nel caso in cui la somma sia un numero molto vicino a zero,
    si mostra l'etichetta "assenti o lievi".
  - Altrimenti, la somma è seguita dalla probabilità delle precipitazioni.
- La temperatura minima e massima per la giornata (tra le 7 e le 23).
- L'umidità minima per la giornata (tra le 7 e le 23). Si mostra il valore minimo
  perché le previsioni giornaliere di OpenWeather mostrano quel dato e si è preferito
  procedere in modo analogo.
- La velocità massima raggiunta dalle folate di vento nella giornata (tra le 7 e le 23).
- Se presenti, le allerte meteo diramate dagli enti locali.

### Quali dati vengono mostrati nelle tabelle orarie?

![Previsione per oggi][images-today]
![Previsione per la notte][images-tonight]

Per le 24 ore successive all'invio del messaggio,
le informazioni mostrate sono le seguenti:

- Previsione meteo.
- Temperatura.
- Velocità media del vento.
- Umidità.
- Quantità di precipitazioni (pioggia e/o neve).
- Indice UV. Vedi la domanda dedicata.
- Indice qualità dell'aria. Vedi la domanda dedicata.

### Quali dati vengono mostrati nella tabella giornaliera?

![Previsione per la settimana][images-week]

Per i sette giorni successivi a quello in cui è stato inviato il messaggio,
le informazioni mostrate sono le seguenti:

- Previsione meteo.
- Temperatura massima e minima.
- Velocità media del vento e direzione.
- Umidità.
- Quantità di precipitazioni (pioggia e/o neve).
- Fase lunare. Vedi la domanda dedicata.

### Quali dati vengono mostrati nella tabella della qualità dell'aria?

![Previsione qualità dell'aria][images-air]

Per le 24 ore successive all'invio del messaggio,
le informazioni mostrate sono le seguenti, raggruppate in fasce bi-orarie:

- Indice qualità dell'aria. Vedi la domanda dedicata.
- Quantità di inquinanti.

### Cosa mostra l'indicatore UV?

L'indice UV viene mostrato come un sole che può assumere i seguenti colori:

- Grigio se l'indice è inferiore a 1.
- Verde se l'indice è inferiore a 3.
- Arancione se l'indice è inferiore a 8.
- Rosso se l'indice è uguale o superiore a 8.

Si raccomanda la lettura delle indicazioni presenti in questo
[articolo di Wikipedia][wikipedia-uv-index] e si raccomanda di applicare
le adeguate protezioni in presenza dell'indicatore arancione o rosso.

### Cosa mostra l'indicatore della qualità dell'aria?

I dati ricevuti dal [servizio di OpenWeather][openweather-air-pollution-api]
riportano i valori dell'[indice CAQI][wikipedia-air-quality-index-caqi],
che vengono mostrati come un palloncino che può assumere i seguenti colori:

- Verde se l'indice è inferiore o uguale a 50.
- Arancione se l'indice è inferiore o uguale a 100.
- Rosso se l'indice è superiore a 100.

### Come viene rappresentata la fase lunare?

La fase lunare viene rappresentata con la seguente iconografia:

- Un cerchio a sfondo nero per la luna nuova.
- Una freccia verso l'alto per la luna crescente.
- Una mezzaluna illuminata a destra per il primo quarto nell'emisfero boreale,
  mentre la mezzaluna è illuminata a sinistra per l'emisfero australe.
- Un cerchio a sfondo bianco per la luna piena.
- Una freccia verso il basso per la luna calante.
- Una mezzaluna illuminata a sinistra per l'ultimo quarto nell'emisfero boreale,
  mentre la mezzaluna è illuminata a destra per l'emisfero australe.

## Contatti

### Ho alcune idee per questo progetto. Come le condivido?

Puoi [aprire una segnalazione][project-new-issue] sul progetto su GitLab.
Oppure, se non hai un account su GitLab o non hai dimestichezza con il servizio,
puoi scrivere una mail a questa casella:

`contact-project+pommalabs-weather-sender-32489905-issue-@incoming.gitlab.com`

### Ho richieste di modifica importanti. Le posso condividere?

Certamente sì, usando i canali indicati sopra. Tuttavia, probabilmente
solo le richieste a basso impatto verranno considerate: se vuoi personalizzare
i messaggi inviati, puoi ripartire dai sorgenti del progetto e modificarli
liberamente in accordo alla [licenza][project-license].

[images-air]: https://alessioparma.xyz/images/weather-sender/air.it.jpg
[images-message]: https://alessioparma.xyz/images/weather-sender/message.it.png
[images-today]: https://alessioparma.xyz/images/weather-sender/today.it.jpg
[images-tonight]: https://alessioparma.xyz/images/weather-sender/tonight.it.jpg
[images-week]: https://alessioparma.xyz/images/weather-sender/week.it.jpg
[openweather-air-pollution-api]: https://openweathermap.org/api/air-pollution
[openweather-geocoding-api]: https://openweathermap.org/api/geocoding-api
[openweather-one-call-api]: https://openweathermap.org/api/one-call-api
[openweather-website]: https://openweathermap.org/
[project-license]: https://gitlab.com/pommalabs/weather-sender/-/blob/main/LICENSE
[project-new-issue]: https://gitlab.com/pommalabs/weather-sender/-/issues/new
[wikipedia-air-quality-index-caqi]: https://en.wikipedia.org/wiki/Air_quality_index#CAQI
[wikipedia-uv-index]: https://it.wikipedia.org/wiki/Indice_UV
