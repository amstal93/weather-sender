#!/bin/bash

set -e

PROJECT_MODULE="weather_sender"
DEFAULT_PORT="8080"

PORT=${PORT:-$DEFAULT_PORT}
CPU_COUNT=$(grep -c "^processor" /proc/cpuinfo)
let WORKER_COUNT=($CPU_COUNT*2 + 1)

poetry run gunicorn $PROJECT_MODULE.main:app \
    --bind 0.0.0.0:$PORT --workers $WORKER_COUNT \
    --worker-class $PROJECT_MODULE.main.CustomUvicornWorker --log-level info
