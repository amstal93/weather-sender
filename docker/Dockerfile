FROM python:3.10-slim-bullseye AS poetry

# Prevents Python from writing pyc files to disc (equivalent to "python -B" option).
ENV PYTHONDONTWRITEBYTECODE=1

# Prevents Python from buffering stdout and stderr (equivalent to "python -u" option).
ENV PYTHONUNBUFFERED=1

ENV PORT=8080
EXPOSE 8080

WORKDIR /opt/app
RUN useradd -m app

# Create writable logs directory:
RUN mkdir -p ./logs
RUN chown -R app:app ./logs

# Install additional packages:
RUN apt-get update -qq && \
    export DEBIAN_FRONTEND=noninteractive && \
    apt-get install -y -qq --no-install-recommends chromium curl dumb-init fonts-firacode && \
    rm -rf /var/lib/apt/lists/* /tmp/*

# Install Poetry:
ENV POETRY_HOME="/opt/poetry" \
    POETRY_VIRTUALENVS_IN_PROJECT="true"
RUN curl -sSL https://install.python-poetry.org | python3 && \
    ln -s ${POETRY_HOME}/bin/poetry /usr/local/bin/poetry

# Poetry project files are required both by the builder and the runner.
# Builder needs them in order to restore dependencies.
# Runner needs them in order to execute commands using Poetry.
COPY ./pyproject.toml ./poetry.lock ./

FROM poetry AS builder

# Install build tools:
RUN apt-get update -qq && \
    export DEBIAN_FRONTEND=noninteractive && \
    apt-get install -y -qq --no-install-recommends build-essential && \
    rm -rf /var/lib/apt/lists/* /tmp/*

# Restore dependencies:
RUN poetry install --no-root --no-dev

FROM poetry AS runner

HEALTHCHECK --interval=30s --timeout=5s \
    CMD curl -f http://localhost:${PORT}/health || exit 1

# Copy init script:
COPY ./docker/container_init.sh /usr/local/bin/container-init
RUN chmod 505 /usr/local/bin/container-init

# Copy restored dependencies:
COPY --from=builder /opt/app/.venv ./.venv

# Copy project files:
COPY ./weather_sender ./weather_sender

# Switch to unprivileged application user:
USER app

# Run dumb-init as main process (https://github.com/Yelp/dumb-init):
ENTRYPOINT ["dumb-init", "--"]

# Start gunicorn server:
CMD ["container-init"]
