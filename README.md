# Weather Sender

[![License: MIT][project-license-badge]][project-license]
[![Donate][paypal-donations-badge]][paypal-donations]
[![Docker Stars][docker-stars-badge]][docker-repository]
[![Docker Pulls][docker-pulls-badge]][docker-repository]

[![standard-readme compliant][github-standard-readme-badge]][github-standard-readme]
[![GitLab pipeline status][gitlab-pipeline-status-badge]][gitlab-pipelines]
[![Quality gate][sonar-quality-gate-badge]][sonar-website]
[![Code coverage][sonar-coverage-badge]][sonar-website]
[![Renovate enabled][renovate-badge]][renovate-website]

Web service which exposes an endpoint to send a Telegram message
with a daily weather forecast for a given location.

Weather Sender is built on [`FastAPI`][github-fastapi] and uses weather data
provided by [OpenWeather][openweather-website].

The idea is to be able to schedule an HTTP call to Weather Sender endpoint
at a desired timing (early morning, e.g. 7 AM). Since the service is available as a Docker image,
it should be easy to setup locally or on cloud providers such as Google Cloud Platform
or Heroku: for example, using Cloud Run with Cloud Scheduler yields a very robust solution
which can cost nothing, if used wisely.

The weather forecast consists of a text message, containing an overview for today
and any potential weather alert emitted by local authorities:

![Telegram message][images-message]

Followed by four images, which contain:

- Hourly forecast for today.
- Hourly forecast for tonight.
- Daily forecast for next seven days.
- Air quality forecast for next 24 hours.

![Forecast for today][images-today]
![Forecast for tonight][images-tonight]
![Forecast for next week][images-week]
![Air quality forecast][images-air]

Please read our [FAQ][project-faq] to find more information about which data is sent.

## Table of Contents

- [Install](#install)
  - [Tags](#tags)
  - [Configuration](#configuration)
    - [`SECURITY_API_KEY`](#security_api_key)
    - [`OPENWEATHER_API_KEY`](#openweather_api_key)
    - [`TELEGRAM_BOT_TOKEN`](#telegram_bot_token)
    - [`TELEGRAM_DISABLE_NOTIFICATION`](#telegram_disable_notification)
    - [`SENDGRID_API_KEY`](#sendgrid_api_key)
    - [`SENDGRID_FROM_EMAIL`](#sendgrid_from_email)
    - [`SENDGRID_TO_EMAILS`](#sendgrid_to_emails)
    - [`HEALTH_CHECK_PING_TIMEOUT`](#health_check_ping_timeout)
  - [Logging](#logging)
- [Usage](#usage)
  - [Send one forecast](#send-one-forecast)
  - [Send many forecasts](#send-many-forecasts)
- [Maintainers](#maintainers)
- [Contributing](#contributing)
  - [Editing](#editing)
  - [Restoring dependencies](#restoring-dependencies)
  - [Starting development server](#starting-development-server)
  - [Running tests](#running-tests)
  - [Building Docker image](#building-docker-image)
- [License](#license)

## Install

Weather Sender web service is provided as a Docker image hosted on [Docker Hub][docker-repository].

You can quickly start a local test instance with following command:

```bash
docker run -it -p 8080:8080 pommalabs/weather-sender:latest
```

Local test instance will be listening on port 8080 and it will accept unauthenticated requests.
Please check the [Configuration](#configuration) section to find further information
about how the web service can be properly configured.

Weather Sender requires an internet connection in order to download weather data
from [OpenWeather][openweather-website] and to send Telegram messages.

### Tags

Following tags are available:

- `latest`, based on `python:3.10-slim`, contains the code found in `main` branch.
- `preview`, based on `python:3.10-slim`, contains the code found in `preview` branch.

Each tag is rebuilt every week.

### Configuration

Docker image can be configured using the following environment variables.
Moreover, the same variables can also be set by mounting a `.env` file in `/opt/app/`.

#### `SECURITY_API_KEY`

Optional API key which should be specified in order to enforce authentication
on incoming HTTP requests. API key might not specified for local setups, but it is
strongly advised to specify it if Weather Sender web service is exposed to the internet.

If API key is specified, then HTTP calls need to include it with the `X-Api-Key` header.

Example:

```bash
# Set the optional API key.
SECURITY_API_KEY='ABC123'
```

#### `OPENWEATHER_API_KEY`

Mandatory environment variable which should contain a valid API key
for OpenWeather APIs. The API key will be used to:

- Retrieve forecast data.
- Reverse geocode specified coordinates, in order to get location data.
- Retrieve air pollution data.

Example:

```bash
# Set the mandatory OpenWeather API key.
OPENWEATHER_API_KEY='ABC123'
```

#### `TELEGRAM_BOT_TOKEN`

Mandatory environment variable which should contain a valid Telegram bot token.
Please follow the [official guide on how to create a bot][telegram-bots]
in order to obtain a valid token.

Example:

```bash
# Set the mandatory Telegram bot token.
TELEGRAM_BOT_TOKEN='ABC123'
```

#### `TELEGRAM_DISABLE_NOTIFICATION`

Controls whether sent messages should trigger a full notification.
Even if set to false, only the first message will trigger a full notification.
Defaults to true, which means that messages will trigger a silent notification.

Example:

```bash
# Do not trigger a full Telegram notification (default).
TELEGRAM_DISABLE_NOTIFICATION='True'

# Trigger a full Telegram notification.
TELEGRAM_DISABLE_NOTIFICATION='False'
```

#### `SENDGRID_API_KEY`

Optional environment variable which should contain a valid API key
for SendGrid APIs. The API key will be used to send an email when weather forecast
could not be sent due to an application error or due to unavailable services.

If API key is not specified, then the email will not be sent; in any case,
the error is logged to console and file system.

Please also see [`SENDGRID_FROM_EMAIL`](#sendgrid_from_email) and [`SENDGRID_TO_EMAILS`](#sendgrid_to_emails).

Example:

```bash
# Set the optional SendGrid API key.
SENDGRID_API_KEY='ABC123'
```

#### `SENDGRID_FROM_EMAIL`

When [SendGrid API key](#sendgrid_from_email) has been specified,
then this environment variable can be used to configure the sender.

Example:

```bash
# Email "from:" address.
SENDGRID_FROM_EMAIL='weather-sender@gmail.com'
```

#### `SENDGRID_TO_EMAILS`

When [SendGrid API key](#sendgrid_from_email) has been specified,
then this environment variable can be used to configure the receivers.

Example:

```bash
# Email "to:" addresses.
SENDGRID_TO_EMAILS='["person1@gmail.com", "person2@gmail.com"]'
```

#### `HEALTH_CHECK_PING_TIMEOUT`

How many seconds should OpenWeather API ping last before failing.
Timeout value should be greater than zero.

```bash
# Set ping timeout to 2 seconds (default).
HEALTH_CHECK_PING_TIMEOUT='2'

# Set ping timeout to 10 seconds.
HEALTH_CHECK_PING_TIMEOUT='10'
```

### Logging

Web service writes log messages to the console and to the file system.
Log files are stored in the following directory, which is not configurable:

`/opt/app/logs`

Log files are rotated daily and a maximum number of 31 files is kept.
A ZIP with all log files can be downloaded through the `/logs` endpoint.

## Usage

Once the web service has been configured and it is running, sending a forecast
requires invoking one of the endpoints described below.

A forecast needs to be sent to a Telegram chat, identified by its ID.
Following articles should help setting up a chat, or a channel, and get its ID:

- [How to create a somewhat "private" bot][telegram-private-bot].
- [How to find a chat or channel ID][telegram-find-chat-id]

### Send one forecast

```http
POST /api/v1/forecasts/send-one HTTP/1.1
Host: localhost:8080
Content-Type: application/json

{
  "latitude": 44.447,
  "longitude": 8.821,
  "language": "it",
  "units": "metric",
  "chat_id": 123456789
}
```

An optional `location_name` property can be added to request object,
which will override the location name found using geocoding.

**Please do not rely on the response data**, because it is just a dump which can be
used to debug the web service logic. It might change over time without notice.

### Send many forecasts

```http
POST /api/v1/forecasts/send-many HTTP/1.1
Host: localhost:8080
Content-Type: application/json

[
  {
    "latitude": 44.447,
    "longitude": 8.821,
    "language": "it",
    "units": "metric",
    "chat_id": 123456789
  },
  {
    "latitude": -33.847,
    "longitude": 150.652,
    "language": "en",
    "units": "imperial",
    "chat_id": 987654321
  }
]
```

An optional `location_name` property can be added to request object,
which will override the location name found using geocoding.

**Please do not rely on the response data**, because it is just a dump which can be
used to debug the web service logic. It might change over time without notice.

## Maintainers

[@pomma89][gitlab-pomma89].

## Contributing

MRs accepted. In any case, the goal of this project is to provide a simple template
from which one could build more advanced or more customized solutions.
Therefore, MRs, which will try to make this web service more complex in order
to make it more "configurable", will not likely be merged.

Small note: If editing the README, please conform to the [standard-readme][github-standard-readme] specification.

### Editing

[Visual Studio Code][vscode-website], with [Remote Containers extension][vscode-remote-containers],
is the recommended way to work on this project.

A development container has been configured with all required tools.

### Restoring dependencies

When starting the development container, dependencies should be automatically restored.

Anyway, this project uses [Poetry][poetry-website] to handle its dependencies:

```bash
poetry install
```

Poetry also manages the virtual environment, which can be activated with following command:

```bash
poetry shell
```

### Starting development server

A local development server listening on port `8080` can be started with following command:

```bash
invoke dev-server
```

### Running tests

Tests have been developed with [pytest][pytest-website] and can be run with following command:

```bash
invoke test-runner
```

Command above runs tests and also collects coverage information.

### Building Docker image

Docker image can be built with following command:

```bash
docker build . -f ./docker/Dockerfile.latest -t $DOCKER_TAG
```

Please replace `$DOCKER_TAG` with a valid tag (e.g. `weather-sender`).

## License

MIT © 2021-2022 [Alessio Parma][personal-website]

[docker-pulls-badge]: https://img.shields.io/docker/pulls/pommalabs/weather-sender?style=flat-square
[docker-repository]: https://hub.docker.com/r/pommalabs/weather-sender
[docker-stars-badge]: https://img.shields.io/docker/stars/pommalabs/weather-sender?style=flat-square
[images-air]: https://alessioparma.xyz/images/weather-sender/air.jpg
[images-message]: https://alessioparma.xyz/images/weather-sender/message.png
[images-today]: https://alessioparma.xyz/images/weather-sender/today.jpg
[images-tonight]: https://alessioparma.xyz/images/weather-sender/tonight.jpg
[images-week]: https://alessioparma.xyz/images/weather-sender/week.jpg
[github-fastapi]: https://github.com/tiangolo/fastapi
[github-standard-readme]: https://github.com/RichardLitt/standard-readme
[github-standard-readme-badge]: https://img.shields.io/badge/readme%20style-standard-brightgreen.svg?style=flat-square
[gitlab-pipeline-status-badge]: https://gitlab.com/pommalabs/weather-sender/badges/main/pipeline.svg?style=flat-square
[gitlab-pipelines]: https://gitlab.com/pommalabs/weather-sender/pipelines
[gitlab-pomma89]: https://gitlab.com/pomma89
[openweather-website]: https://openweathermap.org/
[paypal-donations]: https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=ELJWKEYS9QGKA
[paypal-donations-badge]: https://img.shields.io/badge/Donate-PayPal-important.svg?style=flat-square
[personal-website]: https://alessioparma.xyz/
[poetry-website]: https://python-poetry.org/
[project-faq]: https://gitlab.com/pommalabs/weather-sender/-/blob/main/FAQ.md
[project-license]: https://gitlab.com/pommalabs/weather-sender/-/blob/main/LICENSE
[project-license-badge]: https://img.shields.io/badge/License-MIT-yellow.svg?style=flat-square
[pytest-website]: https://docs.pytest.org/
[renovate-badge]: https://img.shields.io/badge/renovate-enabled-brightgreen.svg?style=flat-square
[renovate-website]: https://renovate.whitesourcesoftware.com/
[sonar-coverage-badge]: https://img.shields.io/sonar/coverage/pommalabs_weather-sender?server=https%3A%2F%2Fsonarcloud.io&sonarVersion=8&style=flat-square
[sonar-quality-gate-badge]: https://img.shields.io/sonar/quality_gate/pommalabs_weather-sender?server=https%3A%2F%2Fsonarcloud.io&sonarVersion=8&style=flat-square
[sonar-website]: https://sonarcloud.io/dashboard?id=pommalabs_weather-sender
[telegram-bots]: https://core.telegram.org/bots
[telegram-find-chat-id]: https://www.alphr.com/find-chat-id-telegram/
[telegram-private-bot]: https://sarafian.github.io/low-code/2020/03/24/create-private-telegram-chatbot.html
[vscode-remote-containers]: https://code.visualstudio.com/docs/remote/containers
[vscode-website]: https://code.visualstudio.com/
