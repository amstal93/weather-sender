# Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
#
# Licensed under the MIT License. See LICENSE file in the project root for full license information.

import string
from collections import namedtuple
from typing import Optional
from unittest.mock import patch
from urllib.parse import parse_qs, urlparse

import requests
from hypothesis import given
from hypothesis.strategies import floats, sampled_from, text
from weather_sender.models import Units
from weather_sender.openweather import (
    MAX_HTTP_TRIES,
    get_air_pollution_data,
    get_location_data,
    get_weather_data,
)
from weather_sender.settings import OpenWeatherSettings

from tests.utils import (
    AIR_POLLUTION_SAMPLES,
    LANGS,
    LOCATION_SAMPLES,
    MAX_LAT,
    MAX_LON,
    MIN_LAT,
    MIN_LON,
    WEATHER_SAMPLES,
    get_stub_openweather_settings,
    load_sample,
    transform_weather_data,
)

API_KEY_LENGTH = 32

Response = namedtuple("Response", ["text"])
EMPTY_RESPONSE = Response("{}")

################################################################################
# Process weather data
################################################################################


# pylint: disable=duplicate-code
@given(
    weather_sample=sampled_from(WEATHER_SAMPLES),
    location_sample=sampled_from(LOCATION_SAMPLES),
    district_name=text(alphabet=string.printable),
    air_pollution_sample=sampled_from(AIR_POLLUTION_SAMPLES),
    lang=sampled_from(LANGS),
)
def test_that_process_weather_data_returns_a_dict(
    weather_sample: str,
    location_sample: str,
    district_name: Optional[str],
    air_pollution_sample: str,
    lang: str,
):
    # Act
    twd = transform_weather_data(
        weather_sample,
        location_sample,
        district_name,
        air_pollution_sample,
        lang,
    )
    # Assert
    assert isinstance(twd, dict)


################################################################################
# Get weather data
################################################################################


@given(
    api_key=text(
        alphabet=string.ascii_letters, min_size=API_KEY_LENGTH, max_size=API_KEY_LENGTH
    ),
    lat=floats(min_value=MIN_LAT, max_value=MAX_LAT),
    lon=floats(min_value=MIN_LON, max_value=MAX_LON),
    lang=sampled_from(LANGS),
    units=sampled_from(Units),
)
def test_that_get_weather_data_uses_input_parameters(
    api_key: str, lat: float, lon: float, lang: str, units: Units
):
    # Arrange
    openweather_settings = OpenWeatherSettings(api_key=api_key)
    with patch.object(
        requests,
        "get",
        return_value=EMPTY_RESPONSE,
    ) as mock:
        # Act
        get_weather_data(openweather_settings, lat, lon, lang, units)
        # Assert
        mock.assert_called_once()
        url = urlparse(mock.call_args.args[0])
        query = parse_qs(url.query)
        assert query["appid"][0] == api_key
        assert query["lat"][0] == str(lat)
        assert query["lon"][0] == str(lon)
        assert query["lang"][0] == str(lang)


# pylint: disable=too-many-arguments
@given(
    api_key=text(
        alphabet=string.ascii_letters, min_size=API_KEY_LENGTH, max_size=API_KEY_LENGTH
    ),
    lat=floats(min_value=MIN_LAT, max_value=MAX_LAT),
    lon=floats(min_value=MIN_LON, max_value=MAX_LON),
    lang=sampled_from(LANGS),
    units=sampled_from(Units),
    sample=sampled_from(WEATHER_SAMPLES),
)
def test_that_get_weather_data_parses_json_response(
    api_key: str, lat: float, lon: float, lang: str, units: Units, sample: str
):
    # Arrange
    openweather_settings = OpenWeatherSettings(api_key=api_key)
    (response_text, parsed_response) = load_sample(sample)
    with patch.object(
        requests,
        "get",
        return_value=Response(response_text),
    ):
        # Act
        weather_data = get_weather_data(openweather_settings, lat, lon, lang, units)
        # Assert
        assert weather_data == parsed_response


# Hypothesis is not used here, because its retries, combined with the retries
# which are tested here, would make this test extremely slow and unreliable.
def test_that_get_weather_data_retries_on_error():
    # Arrange
    openweather_settings = get_stub_openweather_settings()
    with patch.object(
        requests, "get", side_effect=requests.exceptions.RequestException()
    ) as mock:
        # Act
        try:
            get_weather_data(openweather_settings, 0, 0, "en", Units.METRIC)
        except requests.exceptions.RequestException:
            pass
        # Assert
        assert mock.call_count == MAX_HTTP_TRIES


################################################################################
# Get location data
################################################################################


@given(
    api_key=text(
        alphabet=string.ascii_letters, min_size=API_KEY_LENGTH, max_size=API_KEY_LENGTH
    ),
    lat=floats(min_value=MIN_LAT, max_value=MAX_LAT),
    lon=floats(min_value=MIN_LON, max_value=MAX_LON),
)
def test_that_get_location_data_uses_input_parameters(
    api_key: str, lat: float, lon: float
):
    # Arrange
    openweather_settings = OpenWeatherSettings(api_key=api_key)
    with patch.object(
        requests,
        "get",
        return_value=EMPTY_RESPONSE,
    ) as mock:
        # Act
        get_location_data(openweather_settings, lat, lon)
        # Assert
        mock.assert_called_once()
        url = urlparse(mock.call_args.args[0])
        query = parse_qs(url.query)
        assert query["appid"][0] == api_key
        assert query["lat"][0] == str(lat)
        assert query["lon"][0] == str(lon)


@given(
    api_key=text(
        alphabet=string.ascii_letters, min_size=API_KEY_LENGTH, max_size=API_KEY_LENGTH
    ),
    lat=floats(min_value=MIN_LAT, max_value=MAX_LAT),
    lon=floats(min_value=MIN_LON, max_value=MAX_LON),
    sample=sampled_from(LOCATION_SAMPLES),
)
def test_that_get_location_data_parses_json_response(
    api_key: str, lat: float, lon: float, sample: str
):
    # Arrange
    openweather_settings = OpenWeatherSettings(api_key=api_key)
    (response_text, parsed_response) = load_sample(sample)
    with patch.object(
        requests,
        "get",
        return_value=Response(response_text),
    ):
        # Act
        weather_data = get_location_data(openweather_settings, lat, lon)
        # Assert
        assert weather_data == parsed_response


# Hypothesis is not used here, because its retries, combined with the retries
# which are tested here, would make this test extremely slow and unreliable.
def test_that_get_location_data_retries_on_error():
    # Arrange
    openweather_settings = get_stub_openweather_settings()
    with patch.object(
        requests, "get", side_effect=requests.exceptions.RequestException()
    ) as mock:
        # Act
        try:
            get_location_data(openweather_settings, 0, 0)
        except requests.exceptions.RequestException:
            pass
        # Assert
        assert mock.call_count == MAX_HTTP_TRIES


################################################################################
# Get air pollution data
################################################################################


@given(
    api_key=text(
        alphabet=string.ascii_letters, min_size=API_KEY_LENGTH, max_size=API_KEY_LENGTH
    ),
    lat=floats(min_value=MIN_LAT, max_value=MAX_LAT),
    lon=floats(min_value=MIN_LON, max_value=MAX_LON),
)
def test_that_get_air_pollution_data_uses_input_parameters(
    api_key: str, lat: float, lon: float
):
    # Arrange
    openweather_settings = OpenWeatherSettings(api_key=api_key)
    with patch.object(
        requests,
        "get",
        return_value=EMPTY_RESPONSE,
    ) as mock:
        # Act
        get_air_pollution_data(openweather_settings, lat, lon)
        # Assert
        mock.assert_called_once()
        url = urlparse(mock.call_args.args[0])
        query = parse_qs(url.query)
        assert query["appid"][0] == api_key
        assert query["lat"][0] == str(lat)
        assert query["lon"][0] == str(lon)


@given(
    api_key=text(
        alphabet=string.ascii_letters, min_size=API_KEY_LENGTH, max_size=API_KEY_LENGTH
    ),
    lat=floats(min_value=MIN_LAT, max_value=MAX_LAT),
    lon=floats(min_value=MIN_LON, max_value=MAX_LON),
    sample=sampled_from(AIR_POLLUTION_SAMPLES),
)
def test_that_get_air_pollution_data_parses_json_response(
    api_key: str, lat: float, lon: float, sample: str
):
    # Arrange
    openweather_settings = OpenWeatherSettings(api_key=api_key)
    (response_text, parsed_response) = load_sample(sample)
    with patch.object(
        requests,
        "get",
        return_value=Response(response_text),
    ):
        # Act
        weather_data = get_air_pollution_data(openweather_settings, lat, lon)
        # Assert
        assert weather_data == parsed_response


# Hypothesis is not used here, because its retries, combined with the retries
# which are tested here, would make this test extremely slow and unreliable.
def test_that_get_air_pollution_data_retries_on_error():
    # Arrange
    openweather_settings = get_stub_openweather_settings()
    with patch.object(
        requests, "get", side_effect=requests.exceptions.RequestException()
    ) as mock:
        # Act
        try:
            get_air_pollution_data(openweather_settings, 0, 0)
        except requests.exceptions.RequestException:
            pass
        # Assert
        assert mock.call_count == MAX_HTTP_TRIES
