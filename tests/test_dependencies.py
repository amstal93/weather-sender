# Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
#
# Licensed under the MIT License. See LICENSE file in the project root for full license information.

from collections import namedtuple
from unittest.mock import patch

import requests
from fastapi import status
from weather_sender import dependencies
from weather_sender.settings import HealthCheckSettings


from tests.utils import clear_caches, get_stub_logger, get_stub_openweather_settings


################################################################################
# Health checks
################################################################################


FakeResponse = namedtuple("FakeResponse", "status_code")


def test_that_health_check_returns_200_when_ping_website_check_succeeds():
    # Arrange
    clear_caches()
    openweather_settings = get_stub_openweather_settings()
    health_check_settings = HealthCheckSettings()
    with patch.object(
        requests,
        "head",
        return_value=FakeResponse(status_code=status.HTTP_200_OK),
    ):
        # Act
        (_, status_code, _) = dependencies.get_health_check(
            get_stub_logger(),
            openweather_settings,
            health_check_settings,
        ).run()
        # Assert
        assert status_code == status.HTTP_200_OK


def test_that_health_check_returns_503_when_ping_website_check_fails():
    # Arrange
    clear_caches()
    openweather_settings = get_stub_openweather_settings()
    health_check_settings = HealthCheckSettings()
    with patch.object(
        requests,
        "head",
        return_value=FakeResponse(status_code=status.HTTP_503_SERVICE_UNAVAILABLE),
    ):
        # Act
        (_, status_code, _) = dependencies.get_health_check(
            get_stub_logger(),
            openweather_settings,
            health_check_settings,
        ).run()
        # Assert
        assert status_code == status.HTTP_503_SERVICE_UNAVAILABLE
