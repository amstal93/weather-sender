# Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
#
# Licensed under the MIT License. See LICENSE file in the project root for full license information.

import string
from unittest.mock import patch

import pytest
from fastapi import BackgroundTasks, status
from fastapi.testclient import TestClient
from hypothesis import given
from hypothesis.strategies import lists, sampled_from, text
from weather_sender import __version__, dependencies, main
from weather_sender.dependencies import (
    get_logger,
    get_openweather_settings,
    get_telegram_settings,
)
from weather_sender.models import (
    ForecastRequest,
    ForecastRequests,
    ForecastResult,
    ForecastSourceData,
    ForecastTransformedData,
)
from weather_sender.settings import SecuritySettings

from tests.utils import (
    AIR_POLLUTION_SAMPLES,
    LOCATION_SAMPLES,
    STUB_SECURITY_API_KEY,
    WEATHER_SAMPLES,
    build_forecast_request,
    clear_caches,
    get_stub_logger,
    get_stub_openweather_settings,
    get_stub_telegram_settings,
    load_sample,
)

EMPTY_FORECAST_RESULT = ForecastResult(
    source_data=ForecastSourceData(weather={}, location={}, air_pollution={}),
    transformed_data=ForecastTransformedData(weather={}),
)

client = TestClient(main.app)

main.app.dependency_overrides[get_logger] = get_stub_logger
main.app.dependency_overrides[get_openweather_settings] = get_stub_openweather_settings
main.app.dependency_overrides[get_telegram_settings] = get_stub_telegram_settings

################################################################################
# Custom Uvicorn worker
################################################################################


def test_that_custom_uvicorn_worker_config_is_a_dict():
    # Act & Assert
    assert isinstance(main.CustomUvicornWorker.CONFIG_KWARGS, dict)


################################################################################
# Redirects
################################################################################


@pytest.mark.parametrize("endpoint", ["/", "/redoc", "/swagger"])
def test_that_common_docs_endpoint_redirects_to_openapi_docs(endpoint):
    # Act
    response = client.get(endpoint, allow_redirects=False)
    # Assert
    assert response.is_redirect
    assert response.headers["Location"].endswith("/docs")


################################################################################
# Health checks, version and logs
################################################################################


class FakeHealthCheck:
    def __init__(self, content, status_code):
        self.content = content
        self.status_code = status_code

    def run(self):
        return self.content, self.status_code, {"Content-Type": "text/plain"}


@given(content=text(alphabet=string.ascii_letters))
def test_that_health_endpoint_triggers_health_checks(content):
    # Arrange
    clear_caches()
    status_code = status.HTTP_200_OK
    with patch.object(
        dependencies,
        "__get_health_check",
        return_value=FakeHealthCheck(content, status_code),
    ):
        # Act
        response = client.get("/health")
        # Assert
        assert response.text == content
        assert response.status_code == status_code


def test_that_version_endpoint_responds_version():
    # Act
    response = client.get("/version")
    # Assert
    assert response.json() == __version__


def test_that_logs_endpoint_without_security_responds_200():
    # Arrange
    clear_caches()
    with patch.object(
        dependencies,
        "__get_security_settings",
        return_value=SecuritySettings(api_key=None),
    ):
        # Act
        response = client.get("/logs")
        # Assert
        assert response.status_code == status.HTTP_200_OK
        assert len(response.content) > 0


def test_that_logs_endpoint_with_security_without_api_key_responds_403():
    # Arrange
    clear_caches()
    with patch.object(
        dependencies,
        "__get_security_settings",
        return_value=SecuritySettings(api_key=STUB_SECURITY_API_KEY),
    ):
        # Act
        response = client.get("/logs")
        # Assert
        assert response.status_code == status.HTTP_403_FORBIDDEN


################################################################################
# Send one weather forecast
################################################################################

SEND_ONE_ENDPOINT = "/api/v1/forecasts/send-one"


@given(
    request=build_forecast_request(),
    location_sample=sampled_from(LOCATION_SAMPLES),
    weather_sample=sampled_from(WEATHER_SAMPLES),
    air_pollution_sample=sampled_from(AIR_POLLUTION_SAMPLES),
)
def test_that_send_one_weather_forecast_endpoint_reads_openweather_data(
    request: ForecastRequest,
    location_sample: str,
    weather_sample: str,
    air_pollution_sample: str,
):
    # Arrange
    clear_caches()
    (_, location_data) = load_sample(location_sample)
    (_, weather_data) = load_sample(weather_sample)
    (_, air_pollution_data) = load_sample(air_pollution_sample)
    with (
        patch.object(
            dependencies,
            "__get_openweather_settings",
            return_value=get_stub_openweather_settings(),
        ),
        patch.object(
            dependencies,
            "__get_telegram_settings",
            return_value=get_stub_telegram_settings(),
        ),
        patch.object(
            main,
            "get_location_data",
            return_value=location_data,
        ) as mock1,
        patch.object(
            main,
            "get_weather_data",
            return_value=weather_data,
        ) as mock2,
        patch.object(
            main,
            "get_air_pollution_data",
            return_value=air_pollution_data,
        ) as mock3,
        patch.object(main, "send_telegram_message"),
    ):
        # Act
        response = client.post(
            SEND_ONE_ENDPOINT,
            json=request.dict(),
        )
        # Assert
        assert response.status_code == status.HTTP_200_OK
        mock1.assert_called_once()
        mock2.assert_called_once()
        mock3.assert_called_once()


@given(
    request=build_forecast_request(),
    location_sample=sampled_from(LOCATION_SAMPLES),
    weather_sample=sampled_from(WEATHER_SAMPLES),
    air_pollution_sample=sampled_from(AIR_POLLUTION_SAMPLES),
    api_key=text(alphabet=string.ascii_letters),
)
def test_that_send_one_weather_forecast_endpoint_sends_telegram_message(
    request: ForecastRequest,
    location_sample: str,
    weather_sample: str,
    air_pollution_sample: str,
    api_key: str,
):
    # Arrange
    clear_caches()
    (_, location_data) = load_sample(location_sample)
    (_, weather_data) = load_sample(weather_sample)
    (_, air_pollution_data) = load_sample(air_pollution_sample)
    with (
        patch.object(
            dependencies,
            "__get_security_settings",
            return_value=SecuritySettings(api_key=api_key),
        ),
        patch.object(
            dependencies,
            "__get_openweather_settings",
            return_value=get_stub_openweather_settings(),
        ),
        patch.object(
            dependencies,
            "__get_telegram_settings",
            return_value=get_stub_telegram_settings(),
        ),
        patch.object(
            main,
            "get_location_data",
            return_value=location_data,
        ),
        patch.object(
            main,
            "get_weather_data",
            return_value=weather_data,
        ),
        patch.object(
            main,
            "get_air_pollution_data",
            return_value=air_pollution_data,
        ),
        patch.object(main, "send_telegram_message") as mock,
    ):
        # Act
        response = client.post(
            SEND_ONE_ENDPOINT,
            json=request.dict(),
            headers={dependencies.API_KEY_HEADER_NAME: api_key},
        )
        # Assert
        assert response.status_code == status.HTTP_200_OK
        mock.assert_called_once()


def test_that_send_one_weather_forecast_checks_security():
    # Arrange
    clear_caches()
    with patch.object(
        dependencies,
        "__get_security_settings",
        return_value=SecuritySettings(api_key=STUB_SECURITY_API_KEY),
    ):
        # Act
        response = client.post(SEND_ONE_ENDPOINT)
        # Assert
        assert response.status_code == status.HTTP_403_FORBIDDEN


################################################################################
# Send many weather forecasts
################################################################################

SEND_MANY_ENDPOINT = "/api/v1/forecasts/send-many"


@given(requests=lists(build_forecast_request()))
def test_that_send_many_weather_forecasts_endpoint_uses_all_inputs(
    requests: list[ForecastRequest],
):
    # Arrange
    clear_caches()
    with (
        patch.object(
            dependencies,
            "__get_openweather_settings",
            return_value=get_stub_openweather_settings(),
        ),
        patch.object(
            dependencies,
            "__get_telegram_settings",
            return_value=get_stub_telegram_settings(),
        ),
        patch.object(
            main,
            "send_one_weather_forecast",
            return_value=EMPTY_FORECAST_RESULT,
        ) as mock,
    ):
        # Act
        response = client.post(
            SEND_MANY_ENDPOINT,
            json=ForecastRequests(requests=requests, background=False).dict(),
        )
        # Assert
        assert response.status_code == status.HTTP_200_OK
        results = response.json()["results"]
        assert len(results) == len(requests)
        assert mock.call_count == len(requests)


@given(requests=lists(build_forecast_request()))
def test_that_send_many_weather_forecasts_queues_all_inputs_when_background_true(
    requests: list[ForecastRequest],
):
    # Arrange
    clear_caches()
    with (
        patch.object(
            dependencies,
            "__get_openweather_settings",
            return_value=get_stub_openweather_settings(),
        ),
        patch.object(
            dependencies,
            "__get_telegram_settings",
            return_value=get_stub_telegram_settings(),
        ),
        patch.object(
            main,
            "send_one_weather_forecast",
            return_value=EMPTY_FORECAST_RESULT,
        ),
        patch.object(BackgroundTasks, "add_task") as mock,
    ):
        # Act
        response = client.post(
            SEND_MANY_ENDPOINT,
            json=ForecastRequests(requests=requests, background=True).dict(),
        )
        # Assert
        assert response.status_code == status.HTTP_200_OK
        results = response.json()["results"]
        assert len(results) == 0
        assert mock.call_count == len(requests)


def test_that_send_many_weather_forecasts_checks_security():
    # Arrange
    clear_caches()
    with patch.object(
        dependencies,
        "__get_security_settings",
        return_value=SecuritySettings(api_key=STUB_SECURITY_API_KEY),
    ):
        # Act
        response = client.post(SEND_MANY_ENDPOINT)
        # Assert
        assert response.status_code == status.HTTP_403_FORBIDDEN
