# Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
#
# Licensed under the MIT License. See LICENSE file in the project root for full license information.

import string
import subprocess
from typing import Optional
from unittest.mock import patch

from hypothesis import given
from hypothesis.strategies import builds, sampled_from, text, uuids
from weather_sender.templating import (
    AIR_TEMPLATE,
    MESSAGE_TEMPLATE,
    TODAY_TEMPLATE,
    TONIGHT_TEMPLATE,
    WEEK_TEMPLATE,
    convert_html_to_image,
    render_template,
)

from tests.utils import (
    AIR_POLLUTION_SAMPLES,
    LANGS,
    LOCATION_SAMPLES,
    WEATHER_SAMPLES,
    transform_weather_data,
)

TEMPLATES = [
    AIR_TEMPLATE,
    MESSAGE_TEMPLATE,
    TODAY_TEMPLATE,
    TONIGHT_TEMPLATE,
    WEEK_TEMPLATE,
]


class FakeFile:
    def __init__(self, name):
        self.name = str(name)

    def write(self, _):
        # Fake file implementation.
        pass

    def flush(self):
        # Fake file implementation.
        pass


################################################################################
# Render template
################################################################################


# pylint: disable=duplicate-code,too-many-arguments
@given(
    weather_sample=sampled_from(WEATHER_SAMPLES),
    location_sample=sampled_from(LOCATION_SAMPLES),
    district_name=text(alphabet=string.printable),
    air_pollution_sample=sampled_from(AIR_POLLUTION_SAMPLES),
    lang=sampled_from(LANGS),
    template_name=sampled_from(TEMPLATES),
)
def test_that_render_template_returns_non_empty_string(
    weather_sample: str,
    location_sample: str,
    district_name: Optional[str],
    air_pollution_sample: str,
    lang: str,
    template_name: str,
):
    # Arrange
    weather_data = transform_weather_data(
        weather_sample,
        location_sample,
        district_name,
        air_pollution_sample,
        lang,
    )
    # Act
    rendered = render_template(template_name, weather_data)
    # Assert
    assert len(rendered) > 0


################################################################################
# Convert HTML to PGN
################################################################################


# pylint: disable=duplicate-code
@given(
    html_contents=text(),
    html_file=builds(FakeFile, uuids()),
    image_file=builds(FakeFile, uuids()),
)
def test_that_convert_html_to_image_invokes_chromium(
    html_contents: str,
    html_file: FakeFile,
    image_file: FakeFile,
):
    # Arrange
    with patch.object(subprocess, "check_call") as mock:
        # Act
        convert_html_to_image(html_contents, html_file, image_file)
        # Assert
        mock.assert_called_once()
        args = mock.call_args.args[0]
        assert args[0] == "chromium"
        assert any(filter(lambda arg: html_file.name in str(arg), args))
        assert any(filter(lambda arg: image_file.name in str(arg), args))
