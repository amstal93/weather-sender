# Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
#
# Licensed under the MIT License. See LICENSE file in the project root for full license information.

import os
import subprocess

from jinja2 import Environment, FileSystemLoader

from weather_sender.translations import translate

TEMPLATES_PATH = "weather_sender/templates"
AIR_TEMPLATE = "air.html"
MESSAGE_TEMPLATE = "message.txt"
TODAY_TEMPLATE = "today.html"
TONIGHT_TEMPLATE = "tonight.html"
WEEK_TEMPLATE = "week.html"


def __format_number(num: float, precision: int):
    """Rounds given number to specified precision and formats it."""
    return f"{round(num, precision):.{precision}f}"


def __get_resource_file_uri(rel_path):
    """Builds an absolute resource file URI, starting from templates path."""
    return f"file://{os.getcwd()}/{TEMPLATES_PATH}{rel_path}"


__jinja2_env = Environment(loader=FileSystemLoader(TEMPLATES_PATH), autoescape=True)

# Filters:
__jinja2_env.filters["transl"] = translate
__jinja2_env.filters["break"] = lambda txt: txt.replace(" ", "<br/>")
__jinja2_env.filters["fileuri"] = __get_resource_file_uri
__jinja2_env.filters["numfmt"] = __format_number


def render_template(name: str, data: dict):
    template = __jinja2_env.get_template(name)
    return template.render(data)


def convert_html_to_image(html_contents: str, html_file, image_file):
    html_file.write(html_contents)
    html_file.flush()

    subprocess.check_call(
        [
            "chromium",
            "--headless",
            "--disable-gpu",
            "--no-sandbox",
            "--disable-setuid-sandbox",
            "--disable-dev-shm-usage",
            "--hide-scrollbars",
            "--window-size=630,840",
            "--screenshot=" + image_file.name,
            html_file.name,
        ]
    )
