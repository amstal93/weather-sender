# Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
#
# Licensed under the MIT License. See LICENSE file in the project root for full license information.

import functools
import hmac
import logging
import os
from functools import cache
from logging.handlers import TimedRotatingFileHandler

import backoff
import requests
from fastapi import Depends, HTTPException, Security, status
from fastapi.security import APIKeyHeader
from healthcheck import HealthCheck

from weather_sender.settings import (
    HealthCheckSettings,
    OpenWeatherSettings,
    SecuritySettings,
    SendGridSettings,
    TelegramSettings,
)

API_KEY_HEADER_NAME = "X-Api-Key"
LOGS_PATH = "./logs"

api_key_header_auth = APIKeyHeader(name=API_KEY_HEADER_NAME, auto_error=False)


def __get_security_settings():
    return SecuritySettings()


@cache
def get_security_settings():
    return __get_security_settings()


def __get_openweather_settings():
    return OpenWeatherSettings()


@cache
def get_openweather_settings():
    return __get_openweather_settings()


def __get_sendgrid_settings():
    return SendGridSettings()


@cache
def get_sendgrid_settings():
    return __get_sendgrid_settings()


def __get_telegram_settings():
    return TelegramSettings()


@cache
def get_telegram_settings():
    return __get_telegram_settings()


def __get_health_check_settings():
    return HealthCheckSettings()


@cache
def get_health_check_settings():
    return __get_health_check_settings()


async def get_api_key(
    api_key_header: str = Security(api_key_header_auth),
    security_settings: SecuritySettings = Depends(get_security_settings),
):
    if not security_settings.api_key or len(security_settings.api_key) == 0:
        # No API key has been specified;
        # therefore, no security check has to be performed.
        return None

    if not api_key_header:
        raise HTTPException(
            status_code=status.HTTP_403_FORBIDDEN, detail="Missing API key"
        )

    if not hmac.compare_digest(security_settings.api_key, api_key_header):
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Invalid API key",
        )

    return security_settings.api_key


@cache
def get_logger():
    logger = logging.getLogger(__name__)
    uvicorn_logger = logging.getLogger("uvicorn.error")

    # Copy log level from uvicorn logger, which is controlled via command line arguments.
    # If gunicorn is used, its log level has already been copied into uvicorn one.
    logger.setLevel(uvicorn_logger.level)

    # Use handlers of uvicorn logger, since they print beautiful messages to the console.
    # Moreover, by using the same list, additional handlers will be shared
    # (like, for example, the one which writes to file system).
    logger.handlers = (
        uvicorn_logger.handlers
        or (
            # When running directly on unicorn, for example with dev server,
            # handlers are defined on "uvicorn" logger.
            uvicorn_logger.parent
            and uvicorn_logger.parent.name == "uvicorn"
            and uvicorn_logger.parent.handlers
        )
        or []
    )

    file_handler = TimedRotatingFileHandler(
        os.path.join(LOGS_PATH, "app.log"), when="d", backupCount=31, utc=True
    )
    file_handler.setFormatter(
        logging.Formatter("%(asctime)s [%(levelname)s] %(message)s")
    )
    logger.addHandler(file_handler)

    return logger


@backoff.on_exception(backoff.expo, requests.RequestException, max_tries=3)
def __ping_openweather_api(
    logger: logging.Logger,
    openweather_settings: OpenWeatherSettings,
    health_check_settings: HealthCheckSettings,
):
    """Returns True if OpenWeather base URL responds to an HTTP HEAD request."""
    response = requests.head(
        openweather_settings.base_url,
        timeout=health_check_settings.ping_timeout,
        allow_redirects=True,
    )

    if response.status_code < 400:
        return (True, "Ping is OK")

    logger.warning(
        "Ping to '%s' returned %d. Response headers: %s",
        openweather_settings.base_url,
        response.status_code,
        response.headers,
    )
    return (False, "Ping is KO")


def __get_health_check(
    logger: logging.Logger,
    openweather_settings: OpenWeatherSettings,
    health_check_settings: HealthCheckSettings,
):
    health_check = HealthCheck(failed_status=status.HTTP_503_SERVICE_UNAVAILABLE)

    check = functools.partial(
        __ping_openweather_api, logger, openweather_settings, health_check_settings
    )
    setattr(check, "__name__", "ping_openweather_api")
    health_check.add_check(check)

    return health_check


@cache
def get_health_check(
    logger: logging.Logger = Depends(get_logger),
    openweather_settings: OpenWeatherSettings = Depends(get_openweather_settings),
    health_check_settings: HealthCheckSettings = Depends(get_health_check_settings),
):
    return __get_health_check(logger, openweather_settings, health_check_settings)
